﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblLoanNo = New System.Windows.Forms.Label()
        Me.txtLoanNo = New System.Windows.Forms.TextBox()
        Me.lblUB = New System.Windows.Forms.Label()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnCalculate = New System.Windows.Forms.Button()
        Me.lblTotalInterest = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lblLoanNo
        '
        Me.lblLoanNo.AutoSize = True
        Me.lblLoanNo.Location = New System.Drawing.Point(82, 98)
        Me.lblLoanNo.Name = "lblLoanNo"
        Me.lblLoanNo.Size = New System.Drawing.Size(126, 39)
        Me.lblLoanNo.TabIndex = 0
        Me.lblLoanNo.Text = "LoanNo:"
        '
        'txtLoanNo
        '
        Me.txtLoanNo.Location = New System.Drawing.Point(152, 95)
        Me.txtLoanNo.Name = "txtLoanNo"
        Me.txtLoanNo.Size = New System.Drawing.Size(100, 47)
        Me.txtLoanNo.TabIndex = 1
        Me.txtLoanNo.Text = "101848"
        '
        'lblUB
        '
        Me.lblUB.AutoSize = True
        Me.lblUB.Font = New System.Drawing.Font("Calibri", 13.875!, CType(((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic) _
                Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUB.Location = New System.Drawing.Point(12, 26)
        Me.lblUB.Name = "lblUB"
        Me.lblUB.Size = New System.Drawing.Size(841, 45)
        Me.lblUB.TabIndex = 2
        Me.lblUB.Text = "Uncle Buck Finance LLP Total Cost of Credit Calculator"
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(291, 204)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(131, 49)
        Me.btnExit.TabIndex = 3
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnCalculate
        '
        Me.btnCalculate.Location = New System.Drawing.Point(89, 204)
        Me.btnCalculate.Name = "btnCalculate"
        Me.btnCalculate.Size = New System.Drawing.Size(131, 49)
        Me.btnCalculate.TabIndex = 4
        Me.btnCalculate.Text = "Calculate"
        Me.btnCalculate.UseVisualStyleBackColor = True
        '
        'lblTotalInterest
        '
        Me.lblTotalInterest.AutoSize = True
        Me.lblTotalInterest.Location = New System.Drawing.Point(98, 156)
        Me.lblTotalInterest.Name = "lblTotalInterest"
        Me.lblTotalInterest.Size = New System.Drawing.Size(41, 39)
        Me.lblTotalInterest.TabIndex = 5
        Me.lblTotalInterest.Text = "..."
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(16.0!, 39.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(498, 326)
        Me.Controls.Add(Me.lblTotalInterest)
        Me.Controls.Add(Me.btnCalculate)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.lblUB)
        Me.Controls.Add(Me.txtLoanNo)
        Me.Controls.Add(Me.lblLoanNo)
        Me.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "Form1"
        Me.Text = "Uncle Buck Finance LLP Total Cost of Credit Calculator"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblLoanNo As Label
    Friend WithEvents txtLoanNo As TextBox
    Friend WithEvents lblUB As Label
    Friend WithEvents btnExit As Button
    Friend WithEvents btnCalculate As Button
    Friend WithEvents lblTotalInterest As Label
End Class

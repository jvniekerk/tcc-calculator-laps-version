﻿Public Class Form1
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        If MsgBox("Are you sure you want to exit?", vbYesNo + vbQuestion, "Exit TCC Calculator") = 6 Then
            Close()
        End If
    End Sub

    Private Sub btnCalculate_Click(sender As Object, e As EventArgs) Handles btnCalculate.Click
        Dim loanNo As String
        Dim loanTable As DataTable
        Dim rows As Integer
        Dim query As String
        Dim loanID As String
        'Dim testInt As Integer
        Dim failed As Boolean = False
        Dim addIntMessage As String

        'Get the LoanNo entered by the user
        loanNo = txtLoanNo.Text

        'Check the LoanNo is integer
        failed = TestInteger(loanNo)
        'MsgBox(testInt.ToString & " " & failed)

        'Further LoanNo validity checks     '300054
        If failed Then
            MsgBox("Invalid LoanNo, please re-enter a valid LoanNo.", vbOKOnly + vbCritical, "Invalid LoanNo")
        Else    'LoanNo valid
            'Get LoanID
            query = "select LoanID from LAPS.dbo.Loan where LoanNo = " & loanNo.ToString
            'query = "select LoanID from LAPS_uat.dbo.Loan where LoanNo = " & loanNo.ToString
            loanTable = sqlToDataTable(query)
            rows = loanTable.Rows.Count

            'Check if a valid LoanID was returned, i.e. a row was returned
            If rows > 0 Then
                loanID = loanTable.Rows(0).Item(0).ToString
                loanTable.Clear()

                'Get account details to check the user entered the correct LoanNo
                query = "select [First Name] = ln.Lon_FirstName, Surname = ln.Lon_Surname, DOB = convert(varchar(10), ln.Lon_DOB, 103) "
                query += ", [Address Line 1] = convert(varchar(100), REPLACE(ln.Lon_AddL1, ',', ' ')), Town = ln.Lon_Town "
                query += ", Postcode = ln.Lon_Postcode, Balance = -ln.Lon_BalanceCurrent, [Status] = ls.Lst_Desc from Laps.dbo.Loan ln "
                'query += ", Postcode = ln.Lon_Postcode, Balance = -ln.Lon_BalanceCurrent, [Status] = ls.Lst_Desc from Laps_uat.dbo.Loan ln "
                query += "join LAPS.dbo.LoanStatus ls on ln.Lon_LoanStatusID = ls.LoanStatusID where ln.LoanNo = " & loanNo.ToString
                'query += "join LAPS_uat.dbo.LoanStatus ls on ln.Lon_LoanStatusID = ls.LoanStatusID where ln.LoanNo = " & loanNo.ToString
                loanTable = sqlToDataTable(query)
                rows = loanTable.Rows.Count

                If rows > 0 Then
                    With loanTable
                        Dim firstname As String = "First Name: " & .Rows(0).Item(0).ToString
                        Dim surname As String = "Surname: " & .Rows(0).Item(1).ToString
                        Dim dob As String = "Date of Birth: " & .Rows(0).Item(2).ToString
                        Dim add1 As String = "Address Line 1: " & .Rows(0).Item(3).ToString
                        Dim town As String = "Town: " & .Rows(0).Item(4).ToString
                        Dim postcode As String = "Postcode: " & .Rows(0).Item(5).ToString
                        Dim balance As String = "Balance: £" & .Rows(0).Item(6).ToString
                        Dim status As String = "Status: " & .Rows(0).Item(7).ToString
                        balance = Microsoft.VisualBasic.Left(balance, balance.IndexOf(".") + 3)
                        loanTable.Clear()

                        query = "Please check the details below to confirm you have the correct account:" & Environment.NewLine
                        query += Environment.NewLine & firstname
                        query += Environment.NewLine & surname
                        query += Environment.NewLine & dob
                        query += Environment.NewLine & add1
                        query += Environment.NewLine & town
                        query += Environment.NewLine & postcode
                        'query += Environment.NewLine & balance
                        query += Environment.NewLine & status
                        query += Environment.NewLine & Environment.NewLine
                        query += "Are the details above correct?"
                    End With

                    'Get the user to check the account details
                    If MsgBox(query, vbYesNo + vbQuestion, "Please check account details") = 6 Then 'User confirmed correct details
                        lblTotalInterest.Text = "Calculating... Please wait. This could take some time..."
                        lblTotalInterest.Refresh()

                        'Get the interest calculations
                        query = "select * from JosefTestingDB.dbo.fn_GetInterestToAdd (" & loanNo.ToString & ")"
                        'query = "select * from JosefTestingDB_UAT.dbo.fn_GetInterestToAdd (" & loanNo.ToString & ")"
                        loanTable = sqlToDataTable(query)
                        rows = loanTable.Rows.Count

                        'Check interest to add has been returned
                        If rows > 0 Then
                            'Dim contractInt As String = "Contract Interest: £" & loanTable.Rows(0).Item(0).ToString
                            'Dim defaultInt As String = "Default Interest: £" & loanTable.Rows(0).Item(1).ToString
                            Dim totalInt As String = "Total Additional Interest: £" & loanTable.Rows(0).Item(0).ToString
                            Dim transID As String = ""
                            Dim valid As Boolean = False
                            Dim userID As String
                            Dim username As String
                            loanTable.Clear()

                            'Check if any interest is to be added, i.e. the interest amount >0
                            If Double.Parse(Replace(totalInt, "Total Additional Interest: £", "")) = 0 Then
                                lblTotalInterest.Text = "..."
                                lblTotalInterest.Refresh()
                                MsgBox("No additional interest to add to this account has been returned.", vbOKOnly + vbInformation, "No interest to add")
                            Else
                                lblTotalInterest.Text = "LoanNo: " & loanNo.ToString & " --> " & totalInt
                                lblTotalInterest.Refresh()

                                addIntMessage = "Please add the amount of " & Replace(totalInt, "Total Additional Interest: ", "") & " as ONE"
                                addIntMessage += " transaction into LAPS and enter the resulting Transaction No into the box below."

                                While Not valid 'As long as a valid transaction no has not been entered, keep asking for one
                                    failed = False
                                    transID = ""
                                    'Get the transaction no from the user
                                    transID = InputBox(addIntMessage, "Outstanding TCC to be added", 84093)

                                    'Check if the user clicked the cancel button
                                    If transID = vbNullString Then
                                        query = "You've clicked cancel. Please confirm you want to exit without processing the transaction."
                                        query += Environment.NewLine & "If you have already processed a transaction, please complete the process before exiting!"
                                        query += Environment.NewLine & "Do you want to exit without processing a transaction?"

                                        'Check if the customer really wants to exit without completing
                                        If MsgBox(query, vbYesNo + vbQuestion, "Do you want to cancel?") = 6 Then
                                            query = "You will now be returned to the main screen"
                                            MsgBox(query, vbOKOnly + vbInformation, "Exiting transaction")
                                            lblTotalInterest.Text = "..."
                                            lblTotalInterest.Refresh()
                                            Exit Sub
                                        Else
                                            transID = 0
                                        End If
                                    End If

                                    'Check if the transaction no was entered correctly
                                    failed = TestInteger(transID)

                                    'Further transaction no validity checks
                                    If failed Then
                                        query = "The Transaction No entered is not valid. Please enter a valid Transaction No for this transaction."
                                        MsgBox(query, vbOKOnly + vbCritical, "Invalid Transaction No")
                                    Else
                                        'Get the user who processed the transaction & check the transaction relates to the relevant LoanID
                                        query = "select UserID = convert(varchar(10), Trs_CreatedUserID), Username = Usr_Alias, LoanID = Trs_LoanID "
                                        query += " , AmountCleared = Trs_AmountCleared, TransStatus = Trs_TransStatusID from LAPS.dbo.TransSheet "
                                        'query += " , AmountCleared = Trs_AmountCleared, TransStatus = Trs_TransStatusID from LAPS_uat.dbo.TransSheet "
                                        query += " join LAPS.dbo.[User] On Trs_CreatedUserID = UserID where TransSheetNo = " & transID.ToString
                                        'query += " join LAPS_uat.dbo.[User] On Trs_CreatedUserID = UserID where TransSheetNo = " & transID.ToString
                                        loanTable = sqlToDataTable(query)
                                        rows = loanTable.Rows.Count
                                        'MsgBox(Double.Parse(loanTable.Rows(0).Item(3).ToString) & " -> " & Double.Parse(Replace(totalInt, "Total Additional Interest: £", "")))

                                        'Rows were returned, i.e. the transaction was valid
                                        If rows > 0 Then
                                            'Check if the LoanID on the transaction is the same as the one being used
                                            If loanTable.Rows(0).Item(2).ToString <> loanID Then
                                                query = "The Transaction No entered relates To another account. Please enter a New Transaction No."
                                                MsgBox(query, vbOKOnly + vbCritical, "Incorrect Loan")
                                            ElseIf Double.Parse(loanTable.Rows(0).Item(3).ToString) <> Double.Parse(Replace(totalInt, "Total Additional Interest: £", "")) Then
                                                query = "The Transaction amount is not the same as the amount calculated. Please CANCEL the incorrect transaction and"
                                                query += " create a new transaction for the amount of " & Replace(totalInt, "Total Additional Interest: ", "")
                                                MsgBox(query, vbOKOnly + vbCritical, "Incorrect transaction amount")
                                            ElseIf loanTable.Rows(0).Item(4).ToString <> 6 Then
                                                query = "The Transaction you entered did not clear successfully. Please process a new transaction."
                                                MsgBox(query, vbOKOnly + vbCritical, "Transaction not cleared")
                                            Else
                                                userID = loanTable.Rows(0).Item(0).ToString
                                                username = loanTable.Rows(0).Item(1).ToString
                                                loanTable.Clear()

                                                query = "Total Cost Of Credit added: Transaction No " & transID.ToString & " processed by " & username
                                                'query += " (" & userID.ToString & ") and made up of " & contractInt.ToString & " and " & defaultInt.ToString
                                                'query += ", adding up to " & totalInt.ToString()
                                                query += " (" & userID.ToString & "), adding up to " & totalInt.ToString()

                                                ''''''''''Test LoanID
                                                'loanID = "668ae730-6E53-4856-afbc-ea0e97353bac"
                                                '''''''''''Test LoanID

                                                query = "{ ""LoanId"": """ & loanID & """, ""Note"": """ & query & """, ""UserId"": 3"
                                                query += ", ""NoteLevelId"": 0, ""NoteHeaderId"": 316 }"
                                                '''''''''''Test NoteHeaderId
                                                'query += ", ""NoteLevelId"": 0, ""NoteHeaderId"": 300 }"   'Incorrect, same ID on live and UAT as above
                                                '''''''''''Test NoteHeaderId

                                                Dim url As String = "https://api.unclebuck.tv/api/AuxiliaryServices/AddPersonNote?api_key=ckU1eJldjal0SJU3Xed7Ct"
                                                ''''''''''Test UAT URL
                                                'url = "https://auat.unclebuck.tv/api/AuxiliaryServices/AddPersonNote?api_key=ckU1eJldjal0SJU3Xed7Ct"
                                                ''''''''''Test UAT URL

                                                'Send the note request to LAPS and record the response
                                                Dim response As String = sendJson(url, query)
                                                'Clean up the response
                                                response = Replace(Replace(Replace(Replace(response, "{", ""), "}", ""), """:""", ": "), """", "")
                                                'Add a space in front to ensure response.IndexOf("Result: Success") > 0
                                                response = " " & Replace(response, ",", ", ")

                                                'Check and feed the response back to the user
                                                If response.IndexOf("Result: Success") > 0 Then
                                                    MsgBox(response, vbOKOnly + vbInformation, "Note added successfully")
                                                    valid = True
                                                Else
                                                    response += " --> Please try entering the transaction again."
                                                    MsgBox(response, vbOKOnly + vbCritical, "Note not added")
                                                End If
                                            End If
                                        Else
                                            MsgBox("Transaction No Not found. Please Try again.", vbOKOnly + vbCritical, "Invalid Transaction No")
                                        End If
                                    End If
                                End While
                            End If
                        Else
                            lblTotalInterest.Text = "..."
                            lblTotalInterest.Refresh()
                            MsgBox("No additional interest to add to this account has been returned.", vbOKOnly + vbInformation, "No interest to add")
                        End If
                    Else
                        MsgBox("Please Try again.", vbOKOnly + vbCritical, "Incorrect Loan Details")
                    End If
                End If
            Else
                MsgBox("LoanNo Not found. Please Try again.", vbOKOnly + vbCritical, "Invalid LoanNo")
            End If
        End If

        lblTotalInterest.Text = "..."
        lblTotalInterest.Refresh()
    End Sub

    Function TestInteger(ByVal stringToTest As String) As Boolean
        Dim failed As Boolean = False
        Dim testInt As Integer = -1

        If Integer.TryParse(stringToTest, testInt) Then
            failed = False
        Else
            failed = True
        End If
        'MsgBox(testInt.ToString & " " & failed)
        'MsgBox(testInt < 0)
        'MsgBox(Len(stringToTest))

        If failed = False Then
            If Len(stringToTest) < 5 Or stringToTest < 0 Then
                failed = True
            End If
        End If
        'MsgBox(testInt.ToString & " " & failed)

        Return failed
    End Function
End Class

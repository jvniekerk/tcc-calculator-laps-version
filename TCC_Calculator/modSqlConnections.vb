﻿Option Explicit On
Imports System.Data.SqlClient

Module modSqlConnections
    'Function GetConnectionString() As String
    '    Dim hostname = System.Net.Dns.GetHostName()
    '    Dim ip As String = ""
    '    'Dim connString As String = "Initial Catalog=DiallerStagingArea;Data Source="

    '    For Each hostAdr In System.Net.Dns.GetHostEntry(hostname).AddressList()
    '        ip = hostAdr.ToString

    '        If ip = "172.27.63.23" Or ip = "172.27.63.17" Then
    '            Return "Initial Catalog=Laps;Data Source=UB-SQLCLSTR-1;User ID=SA;Password=u3zD2Ix6;Connect Timeout=0;Connection Timeout=0;Integrated Security=False;"
    '            '    connString += "UB-SQLCLSTR-1"
    '            'Else
    '            '    connString += "172.27.63.23"
    '        End If
    '    Next hostAdr

    '    'connString += ";User ID=SA;Password=u3zD2Ix6;Connect Timeout=0;Connection Timeout=0;Integrated Security=False;"

    '    Return "Initial Catalog=Laps;Data Source=172.27.63.23;User ID=SA;Password=u3zD2Ix6;Connect Timeout=0;Connection Timeout=0;Integrated Security=SSPI;"
    '    'Return connString
    'End Function

    Function sqlToDataTable(ByVal sqlQuery As String) As DataTable
        Dim myConn As SqlConnection
        Dim myCmd As SqlCommand
        Dim myReader As SqlDataReader
        Dim results As New DataTable


        'Dim connStr As String '"Server=172.27.63.23;Database=Automation;User Id=SA;Password=u3zD2Ix6"
        'connStr = "Initial Catalog=Laps;Data Source=172.27.63.23;User ID=SA;Password=u3zD2Ix6;Connect Timeout=0;Connection Timeout=0;Integrated Security=False;"
        '''''''''''Test AUT connection string
        ''connStr = "Initial Catalog=LAPS_UAT;Data Source=172.27.63.53;User ID=LUATREAD;Password=AF#8csV6;Connect Timeout=0;Connection Timeout=0;Integrated Security=False;"
        '''''''''''Test AUT connection string




        Dim dataSource = "ub-avgr01.unclebuck.ukfast"
        Dim user = "TTCCalculator"
        Dim pw = "0097*^&56564""ol"


        'Dim dataSource = "172.27.63.23"
        'Dim user = "sa"
        'Dim pw = "u3zD2Ix6"

        Dim connStr = "Initial Catalog=DiallerStagingArea;Data Source=" & dataSource & ";User ID=" & user & ";Password=" & pw & ";Connect Timeout=0;Connection Timeout=0;Integrated Security=False;"


        myConn = New SqlConnection(connStr)
        myCmd = myConn.CreateCommand
        myCmd.CommandTimeout = 0
        myCmd.CommandText = sqlQuery

        Try
            myConn.Open()
            myReader = myCmd.ExecuteReader()
            results.Load(myReader)

            myReader.Close()
            myReader = Nothing

            myConn.Close()
            myConn = Nothing
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return results
    End Function
End Module

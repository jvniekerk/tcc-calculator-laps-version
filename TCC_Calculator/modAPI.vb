﻿Module modAPI

    Public Function sendJson(ByVal url As String, ByVal envelope As String) As String
        Dim ObjHTTP As Object = CreateObject("MSXML2.ServerXMLHTTP")

        With ObjHTTP
            .open("POST", url, False)
            .setRequestHeader("Content-Type", "application/json; charset=utf-8")
            .setTimeouts(60000, 60000, 1200000, 1200000)
            .send(envelope)

            Dim myString As String = .responseText
            Dim status As Integer = .status.ToString()

            If status = 200 Then
                Return myString
            Else
                Return "Status code: " & status
            End If

            sendJson = myString
        End With
        ObjHTTP = Nothing

        Return sendJson
    End Function
End Module
